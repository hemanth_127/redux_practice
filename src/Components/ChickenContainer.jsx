import React from 'react'
import { connect } from 'react-redux'
import { buyChicken } from '../redux'

const ChickenContainer = props => {
  return (
    <div className='chicken-Container'>
      <h2>number of MEAT {props.numberOfChickens}</h2>
      <button onClick={props.buyChicken}>ADD</button>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    numberOfChickens: state.chicken.numberOfChickens
  }
}

const mapDispatchToProps = dispatch => {
  return {
    buyChicken: () => dispatch(buyChicken())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChickenContainer)
