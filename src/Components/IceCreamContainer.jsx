import React from 'react'
import { buyIceCream } from '../redux'
import { connect } from 'react-redux'

const IceCreamContainer = props => {
  return (
    <div className='IceCream'>
      <h2>Number of IceCream -{props.numberOfIceCreams} </h2>
      <button onClick={props.buyIceCream}>Buy IceCream</button>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    numberOfIceCreams: state.iceCream.numberOfIceCreams
  }
}

const mapDispatchToProps = dispatch => {
  return {
    buyIceCream: () => dispatch(buyIceCream())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IceCreamContainer)
