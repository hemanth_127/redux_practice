import React, { useState } from 'react'
import { connect } from 'react-redux'
import { buyChicken } from '../redux'

const NewChickenContainer = (props) => {
  const [number, setNumber] = useState(1)

  return (
    <div className='chicken-Container'>
      <h2>number of MEAT {props.numberOfChickens}</h2>
      <input
        type='text'
        value={number}
        onChange={e => setNumber(e.target.value)}
      />
      <button onClick={()=>props.buyChicken(number)}>ADD {number}</button>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    numberOfChickens: state.chicken.numberOfChickens
  }
}

const mapDispatchToProps = dispatch => {
  return {
    buyChicken: number => dispatch(buyChicken(number))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewChickenContainer)
