import './App.css'

import { Provider } from 'react-redux'
import store from './redux/store'
import CakeContainer from './Components/CakeContainer'
import IceCreamContainer from './Components/IceCreamContainer'
import HooksCakeContainer from './Components/HooksCakeContainer'
import ChickenContainer from './Components/ChickenContainer'
import NewChickenContainer from './Components/NewChickenContainer'
import ItemContainer from './Components/ItemContainer'
import UserContainer from './Components/UserContainer'

function App () {
  return (
    <Provider store={store}>
      <div className='App'>
        <UserContainer />
        {/* <ItemContainer cake />
        <ItemContainer />
        <ChickenContainer />
        <NewChickenContainer />
        <CakeContainer />
        <HooksCakeContainer />
        <IceCreamContainer /> */}
      </div>
    </Provider>
  )
}

export default App
