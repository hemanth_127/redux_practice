import { BUY_CHICKEN } from './chickenType'

const initialState = {
  numberOfChickens: 1000
}

const chickenReducer = (state = initialState, action) => {
  switch (action.type) {
    case BUY_CHICKEN:
      return {
        ...state,
        numberOfChickens: state.numberOfChickens + action.payload
      }
    default:
      return state
  }
}

export default chickenReducer
