import { BUY_CHICKEN } from './chickenType'

export const buyChicken = (number = 1) => {
  return {
    type: BUY_CHICKEN,
    payload: parseInt(number)
  }
}
