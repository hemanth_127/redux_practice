import { combineReducers } from 'redux'

import cakeReducer from './cakes/cakeReducer'
import IceCreamReducer from './IceCreams/IceCreamReducer'
import chickenReducer from './Chicken/chickenReducer'
import userReducer from './user/userReducer'

const rootReducer = combineReducers({
  cake: cakeReducer,
  iceCream: IceCreamReducer,
  chicken: chickenReducer,
  user: userReducer
})

export default rootReducer
