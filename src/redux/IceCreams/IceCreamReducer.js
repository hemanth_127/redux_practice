import { BUY_ICECREAM } from './IceCreamType'

// Reducer
const initialState = {
  numberOfIceCreams: 30
}

const IceCreamReducer = (state = initialState, action) => {
  switch (action.type) {
    case BUY_ICECREAM:
      return {
        ...state,
        numberOfIceCreams: state.numberOfIceCreams - 1
      }
    default:
      return state
  }
}

export default IceCreamReducer
